module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        "nerd-primary": "#7f39fb",
        "nerd-secondary": "#ff5f1e",
      },
    },
  },
  plugins: [],
};
